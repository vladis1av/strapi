"use strict";
const { sanitizeEntity } = require("strapi-utils");

module.exports = {
  async create(ctx) {
    let entity;
    const post = ctx.request.body;
    post.user = ctx.state.user.id;
    entity = await strapi.services.posts.create(post);
    return sanitizeEntity(entity, { model: strapi.models.posts });
  },
};
